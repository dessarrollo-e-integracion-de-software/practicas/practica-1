package es.dis.practica1.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionsDto {

    private String seriesReference;
    private Date period;
    private Float dataValue;
    private Character status;
    private String units;
    private String subject;
    private String group;
    private String seriesTitle1;
    private String seriesTitle2;

}
