package es.dis.practica1.model;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import es.dis.practica1.dto.TransactionsDto;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class TransactionList {

    private ArrayList<TransactionsDto> list;

    private static TransactionList transactionList;

    private TransactionList() {
        list = new ArrayList<TransactionsDto>();
        initialize();
    }

    public static TransactionList getInstance(){
        if(transactionList == null)
            transactionList = new TransactionList();
        return transactionList;
    }


    /**
     * Lee el fichero y lo va almacenando en la lista de transacciones
     */
    private void initialize() {

        try{
            CSVReader reader = new CSVReader(new FileReader("src/main/resources/electronic_card_transactions.csv"));
            String[] nextLine;

            while((nextLine = reader.readNext()) != null){
                TransactionsDto dto = new TransactionsDto();
                dto.setSeriesReference(nextLine[0]);
                dto.setPeriod(new SimpleDateFormat("YYYY.MM").parse(nextLine[1]));
                dto.setDataValue(Float.parseFloat(nextLine[2]));
                dto.setStatus(nextLine[4].charAt(0));
                dto.setUnits(nextLine[5]);
                dto.setSubject(nextLine[6]);
                dto.setGroup(nextLine[7]);
                dto.setSeriesTitle1(nextLine[8]);
                dto.setSeriesTitle2(nextLine[9]);

                list.add(dto);
            }
        }catch (FileNotFoundException ex){
            System.err.println("Error al leer el fichero: " + ex.getMessage());
        } catch (CsvValidationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException e){
            e.printStackTrace();
        }


    }
}
